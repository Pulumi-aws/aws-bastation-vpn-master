AWS :cloud:, Pulumi :wrench:, Python :snake: and CI/CD :fox: VPN :lock:, VPC.

**Intro**

This project is the extension of the previous projetc `AWS-VPC` can be found in [AWS-VPC]( https://gitlab.com/ehsan.tafehi/aws-EC2-CICD.git) and illustrated as `VPC structure`

                                                     ** VPC structure**


![VPC structure](Structure01.PNG)


At this stage one private instance is added to:
- Create a VPN connection to another corporation using VPN and IPsec protocol.
- Create a endpoint in order to connect to AWS S3 via this Instacne.
- This approach can be seen as `VPN & secure connection to AWS S3`

                                                    ** VPN & secure connection to AWS S3**




![VPN & secure connection to AWS S3](Structure02.PNG)

 
 
 **Steps and benefits:**
- The second private instance is added.
- A policy role is created and attached to this instance to access the AWS S3 via Endpoint.
- A VPN gateway, Site to site connection and customer gateway are created and connected to second instance.

- Finally:

- [X] The second private instacne is able to reach S3 on AWS without internet connection and via internal connection.
- [X] The VPN gateway create a secure point to point connection by IPsec protocol





**Architecture**:

- The code is writen in Pycharm :snake:
- then the code is pusshed into Gitlab and by using CI/CD it is feed into the pipeline.
- initially it installs Pulumi, Python and awscli.
- The the pipeline preview the whole VPC infrastructure.
- if the preview is successful then at the next stage of the pipeline it runs and deploy the VPC on AWS.

                                                     **Architecture**

![Architecture](architecture.png)


**Notes:** :speech_balloon:

I have used AWS account for the purpuse of this project.

if you want to use your own SSH keys just change the name of the KeyPair in the code to you desired key.

Feel free to run `pulumi destroy --yes` command to delete all of deployed infrastructure.

the whole IaC steps are automatized. Only AWS credentials need to be set in order for the pipeline to run successfully
