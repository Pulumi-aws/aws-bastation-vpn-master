import pulumi

from pulumi import Output
import pulumi_aws as aws


#This function is created inorder to change the bahaviour of the List in Pulumi Env.!!

# Bash for installing apache webserver
user_data = """
        #!/bin/bash

        # get admin privileges
        sudo su

        # install httpd (Linux 2 version)
        apt update -y
        aot install -y httpd.x86_64
        systemctl start apache2
        systemctl enable apache2
        echo "Hello World from $(hostname -f)" > /var/www/html/index.html 
        """
# Creating the desired VPC
vpc = aws.ec2.Vpc('AWS-VPC',
              cidr_block="10.13.0.0/16",
              enable_dns_hostnames=True,
              enable_dns_support=True, tags={"Name": "AWS-VPC"})

# Creating Internet gatway for private subnet
internet_gateway = aws.ec2.InternetGateway('InternetGway',
                                       vpc_id=vpc.id, tags={"Name": "InternetGateway-Pulic-Server"})
# EIP for NAT gateway && NAT Gateway for internet access in private Subnet
eip = aws.ec2.Eip("Eip-Internet", vpc=True)

# getting the avalibility zones (AZs)
zones = Output.from_input(aws.get_availability_zones())
zone_names = zones.apply(
    lambda zs: zs.names)


# creating first subnet base on the first AZs
for i in range(0,3):
    if i < 1:
        SubnetId=(aws.ec2.Subnet("Public-Subnet-"+str(i),
                         vpc_id=vpc.id,
                         availability_zone=zone_names.apply(lambda names: names[0]),
                         cidr_block="10.13."+str(i)+".0/24",
                         map_public_ip_on_launch=True, tags={"Name": "Subnet-Pulic-Server-"+str(i)}))
        # Route table for public Subnet
        route_table = aws.ec2.RouteTable('RouteT-Public-'+str(i),
                                      vpc_id=vpc.id,
                                      routes=[aws.ec2.RouteTableRouteArgs(
                                          cidr_block="0.0.0.0/0",
                                          gateway_id=internet_gateway.id
                                      )], tags={"Name": "Public-RTable-"+str(i)}, )
        # Route table accociates for public subnet
        route_table_association = aws.ec2.RouteTableAssociation('Public-RouteTable-'+str(i),
                                                             subnet_id=SubnetId.id,
                                                             route_table_id=route_table.id, )
        # public instance security group, port 22 for SSH and 80 for HTTP is set as inbound
        group = aws.ec2.SecurityGroup(
            "Sg-Public-Server-"+str(i),
            description="Enable access",
            vpc_id=vpc.id,
            ingress=[
                {
                    "protocol": "tcp",
                    "from_port": 22,
                    "to_port": 22,
                    "cidr_blocks": ["0.0.0.0/0"],
                },
                {
                    "protocol": "tcp",
                    "from_port": 80,
                    "to_port": 80,
                    "cidr_blocks": ["0.0.0.0/0"],
                },
            ],
            egress=[
                {"protocol": "-1", "from_port": 0, "to_port": 0, "cidr_blocks": ["0.0.0.0/0"], }
            ],
            tags={"Name": "Sg-Public-Server-"+str(i)},
        )
        # Create the public webserver
        server = aws.ec2.Instance(
            "Public-Server-"+str(i),
            instance_type='t2.micro',
            subnet_id=SubnetId.id,
            vpc_security_group_ids=[group.id],
            user_data=user_data,
            ami='ami-0502e817a62226e03',
            key_name='aws_Key',
            tags={"Name": "Public-Server-"+str(i)},
        )


    else: #Adjusting the CIDR arneg to avoid conflict
            # creating second subnet base on the second AZ and for  VPN and network access
            SubnetId=(aws.ec2.Subnet("Private-Subnet-"+str(i),
                                 vpc_id=vpc.id,
                                 availability_zone=zone_names.apply(lambda names: names[1]),
                                 cidr_block="10.13."+str(i)+".0/28",
                                map_public_ip_on_launch=True, tags={"Name": "Subnet-Private-Server-"+str(i)})
                            )
            # private instance security group, port 22 with CIDR of public instance is set
            # so only SSH can be done when you are inside public instance
            group = aws.ec2.SecurityGroup(
                "Sg-Private-Server-" + str(i),
                vpc_id=vpc.id,
                description="Enable access",
                ingress=[
                    {
                        "protocol": "tcp",
                        "from_port": 22,
                        "to_port": 22,
                        "cidr_blocks": ["10.13.0.0/24"],
                    },
                    {
                        "protocol": "tcp",
                        "from_port": 0,  # port fo ICMP
                        "to_port": 65535,
                        "cidr_blocks": ["10.13.0.0/24"],
                    },
                ], egress=[
                    {"protocol": "-1", "from_port": 0, "to_port": 0, "cidr_blocks": ["0.0.0.0/0"], }
                ], tags={"Name": "Sg-Private-Server-" + str(i)},
            )
            if i == 1: #Assign only NAT gateway to the first private instacne
                NATGway = aws.ec2.NatGateway("Private-Internet-"+str(i),
                                         allocation_id=eip.id,
                                         subnet_id=SubnetId.id, tags={"Name": "Private-NAT-"+str(i)}, )

                # Route table for private subnet
                route_table = aws.ec2.RouteTable('RouteT-Private-'+str(i),
                                              vpc_id=vpc.id,
                                              routes=[aws.ec2.RouteTableRouteArgs(
                                                  cidr_block="0.0.0.0/0",
                                                  gateway_id=NATGway.id
                                              )], tags={"Name": "Private-RTable-"+str(i)}, )
                # Route table accociates for private subnet
                route_table_association = aws.ec2.RouteTableAssociation('Private-RouteTable-'+str(i),
                                                                     subnet_id=SubnetId.id,
                                                                     route_table_id=route_table.id, )

            elif i ==2: #assign Net access and VPN to Second private Insatnce

                role = aws.iam.Role("S3-FullAcess-Role",
                  assume_role_policy="""{
                  "Version": "2012-10-17",
                  "Statement": [
                    {
                      "Action": "sts:AssumeRole",
                      "Principal": {
                        "Service": "ec2.amazonaws.com"
                      },
                      "Effect": "Allow",
                      "Sid": "GrantS3Access"
                    }
                  ]
                }

                """,
                    tags={
                           "tag-key": "tag-value",
                                         })
                policy = aws.iam.Policy("AmazonS3FullAccess",
                            description="S3 Full access policy",
                            path="/",
                            policy="""{
                          "Version": "2012-10-17",
                          "Statement": [
                            {
                              "Action": [
                                "s3:*"
                              ],
                              "Effect": "Allow",
                              "Resource": "*"
                            }
                          ]
                        }
        
                        """)
                Instance_profile = aws.iam.InstanceProfile("EC2-S3FullAccess-Profile", role=role.name)

                s3_attach = aws.iam.RolePolicyAttachment("EC2-S3-attach",
                                                           role=role.name,
                                                           policy_arn="arn:aws:iam::aws:policy/AmazonS3FullAccess")
                                                           # OR in policy_arn USE: arn:aws:iam::aws:policy/AmazonS3FullAccess


                VPNGW = aws.ec2.VpnGateway("vpnGw",
                                            vpc_id=vpc.id,
                                            tags={
                                                "Name": "vpnGw",
                                            })
                # Route table for private subnet
                route_table = aws.ec2.RouteTable('RouteT-Private-' + str(i),
                                                 vpc_id=vpc.id,propagating_vgws=[VPNGW], tags={"Name": "Private-RTable-" + str(i)}, )

                # Route table accociates for private subnet
                route_table_association = aws.ec2.RouteTableAssociation('Private-RouteTable-' + str(i),
                                                                        subnet_id=SubnetId.id,
                                                                        route_table_id=route_table.id, )
                VPNEndpint=aws.ec2.VpcEndpoint("S3-Endpint",
                    vpc_id=vpc.id,
                    route_table_ids=[route_table.id],
                    service_name="com.amazonaws.eu-central-1.s3",
                    vpc_endpoint_type="Gateway",
                    tags={
                                                "Name": "S3-Endpint",
                                            })
                CustomerGW = aws.ec2.CustomerGateway("CustomerGW",
                                               bgp_asn="65000",
                                               ip_address="172.83.124.10",
                                               tags={
                                                   "Name": "Customer-gateway",
                                               },
                                               type="ipsec.1")
                #VPN COnnection
                VPN=aws.ec2.VpnConnection("VPN-Conn",
                                      customer_gateway_id=CustomerGW.id,
                                        vpn_gateway_id=VPNGW.id,
                                      type="ipsec.1")


                # Create the private webserver Az 2 with IAM Policy to access to S3
                server = aws.ec2.Instance(
                    "Private-Server-" + str(i),
                    instance_type='t2.micro',
                    iam_instance_profile=Instance_profile.name, #attache the IAM role to Instance
                    subnet_id=SubnetId.id,
                    vpc_security_group_ids=[group.id],
                    user_data=user_data,
                    ami='ami-0502e817a62226e03',
                    key_name='aws_Key',
                    tags={"Name": "Private-Server-" + str(i)},
                )
                break





            # Create the private webserver Azs
            server = aws.ec2.Instance(
                "Private-Server-"+str(i),
                instance_type='t2.micro',
                subnet_id=SubnetId.id,
                vpc_security_group_ids=[group.id],
                user_data=user_data,
                ami='ami-0502e817a62226e03',
                key_name='aws_Key',
                tags={"Name": "Private-Server-"+str(i)},
            )




















